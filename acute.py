#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

import mesh
import fvm
import math
import numpy as np
from matplotlib import pyplot as plt
from collections import defaultdict
from termcolor import colored
from os import system

#import test_micromodel as tm

#
# Attempt to remove obtuse angles inside a mesh using method discribed in:
# https://www.sciencedirect.com/science/article/pii/S0898122117306211
#
# For new we attempt to create an acute mesh without iterating on the
# process: it asks to have very regular edge sizes (as a too long edge
# along the boundary cavity (comparing to other edges) will make
# impossible to insert a node without recreating large angles)
#

# face indices
indices = np.array([1, 2, 3, 0, 2, 3, 0, 1, 3, 0, 1, 2], dtype=np.int32).reshape([4, 3])


def construct_adja(Th):
    """Construct the array of the d+1 neighbours of the elements such as:
    if element k is adjacent to l through local edge i in k and j in l,
    then adja[k,i] = [l,j]
    """
    ## Remark: not tested in 3D

    # Get the elements and the dimension of the interfaces
    if Th.dim == 2 or Th.is_surface:
        dim = 2
        elt = Th.triangle
        n_elts = Th.n_triangles
    else:
        dim = 3
        elt = Th.tetra
        n_elts = Th.n_tetras

    # Create a hash table to store the adjacency information
    adja_table = {}

    # Iterate over all elts
    v = np.empty((dim+1), dtype=np.int32)
    for i in range(n_elts):

        # skip deleted elts
        if elt[i,0] == -1:
            continue

        # Iterate over all faces of the elts
        for j in range(dim+1):
            # Get the indices of the face of the edge
            for k in range(dim):
                v[k] = elt[i, indices[j,k]]

            # Create a key for the edge using a frozenset
            key = frozenset(v)
            # Check if the key already exists in the hash table
            if key in adja_table:
                # If the key exists, add the current triangle to the set of adjacent triangles
                adja_table[key].add(i)
            else:
                # If the key does not exist, create a new entry in the hash table
                adja_table[key] = {i}

    # Create a numpy array to store the adjacency information
    adja = np.full((n_elts, dim+1), -1, dtype=np.int32)
    for i in range(n_elts):
        # skip deleted elts
        if elt[i,0] == -1:
            continue

        for j in range(dim+1):
            # Get the indices of the two vertices of the edge
            for k in range(dim):
                v[k] = elt[i, indices[j,k]]
                # Create a key for the edge using a frozenset
            key = frozenset(v)
            # Get the set of adjacent triangles from the hash table
            adja_triangles = adja_table[key] - {i}
            # If there is exactly one adjacent triangle, store its
            # index in the adjacency array
            if len(adja_triangles) == 1:
                adja[i, j] = next(iter(adja_triangles))

    return adja


def sort_tria_edges_by_length(Th,elt_verts):
    """Sort edges of an element by length and return the sorted array
    of edge indices (with following convention: edge 0 has vertices
    1-2, edge 1: 2-0, edge 2: 0-1

    """

    v0 = elt_verts[0]
    v1 = elt_verts[1]
    v2 = elt_verts[2]

    h0 = np.linalg.norm(Th.node[v1] - Th.node[v2])
    h1 = np.linalg.norm(Th.node[v0] - Th.node[v2])
    h2 = np.linalg.norm(Th.node[v1] - Th.node[v0])

    h = np.array([h0,h1,h2],dtype=np.float32)

    sorted_idx = np.argsort(-h)

    return sorted_idx

def select_neighbours(Th,adja,elt_idx,exclude_idx=-1):
    """Return possible neighbours of element elt_idx ordered by
    success probablility. The best neighbour is the longest
    non-boundary edge (note that an edge at the interface of 2 domains
    with different references is considered as boundary).  Fill array
    by -1 values if some neighbours are not admissible (because we
    have to cross a boundary).
    Ignore the neighbour matching the exclude_idx index if provided.
    """

    if exclude_idx==-1:
        n_neigh = 3
    else:
        n_neigh = 2

    neigh = np.full(n_neigh,-1,np.int32)

    elt_verts = Th.triangle[elt_idx,:]
    sorted_idx = sort_tria_edges_by_length(Th,elt_verts)

    ref = Th.triangle_label[elt_idx]

    k = 0
    for i in range(3) :
        adj_idx = adja[elt_idx,sorted_idx[i]]

        # edge is an external boundary
        if adj_idx == -1:
            continue

        if adj_idx == exclude_idx:
            continue

        adj_ref = Th.triangle_label[adj_idx]

        # edge is an internal boundary
        if adj_ref != ref:
            continue

        # edge is internal: neighbour can be added to cavity
        neigh[k] = adj_idx
        k+=1

    return neigh

def compute_cavities(Th,adja,elt_idx):
  """Compute the 6 possible cavities for the element.  Each cavity
     contains the element, its first neighbour and its second
     neighbour (neighbour to the first neighbour) and cannot cross
     boundary edges. Other possibilities for the cavity is to choose
     two first neighbours (not implemented in the paper that has
     inspired this code).
     Cavities are listed in order of probability
     of success: try first to choose neighbours through longest edges.
     Convention is to store -1 index if the cavity cross a boundary
     edge.

  """
  # Remark: for sake of simplicity in the implementation, there are
  # redundencies in the cavities listing (cavities formed by the 2
  # first neighbours of the element are listed twice)
  cavities = np.full((12,3),-1, dtype=np.int32)

  cavities[:,0] = elt_idx
  cavities[:,1] = np.repeat(select_neighbours(Th,adja,elt_idx),4)

  for k in range(3):
      kk = cavities[4*k,1]

      if  kk == -1 :
          continue

      cavities[4*k:4*k+2,2]   = select_neighbours(Th,adja,kk,elt_idx)
      cavities[4*k+2:4*k+4,2] = select_neighbours(Th,adja,elt_idx,kk)

  if cavities[:,2].max() == -1 and verbosity > 1:

      print(colored(f"      WARNING: no valid cavity for element {elt_idx}",'yellow'))

  return cavities

def find_cycle(start_vertex, graph, visited, parent):
    """Travel graph from a given node until retrieving this node.
    Return the list of graph nodes that have been visited to get back
    to starting vertex.

    """

    visited.append(start_vertex)
    for v in graph[start_vertex]:
        if v not in visited:
            result = find_cycle(v, graph, visited, start_vertex)
            if result:
                return result
        elif v != parent:
            return visited[visited.index(v):]

def permute_cycle(edg_list):
    """
    Permute edge vertices and edges of edg_list to create a cycle.
    Return the list of ordered vertex indices in 'result' and the
    ordered edges in 'cycle'.
    """

    # Dictionary creation
    graph = defaultdict(list)
    for v1, v2 in edg_list:
        graph[v1].append(v2)
        graph[v2].append(v1)

    for start_vertex in graph.keys():
        # Search cycle inside the edge list from vertex
        # start_vertex. Normally we should success at first loop
        # iteration.
        visited = []
        result = find_cycle(start_vertex, graph, visited, None)
        if result:
            cycle = [[result[i], result[i+1]] for i in range(len(result)-1)]
            cycle.append([result[-1], result[0]])
            result.append(result[0])
            return result,cycle

def compute_cavity_boundaries(Th,cavity):
    """List the boundary edges of the cavity and return the edge list
    and the coordinates of the cavity boundary points"""

    edg_list = []
    for k in range(cavity.shape[0]) :
        elt_idx = cavity[k]
        v = Th.triangle[elt_idx,:]

        for i in range(3):
            # Get the indices of the two vertices that make up this edge
            v1 = Th.triangle[elt_idx,i]
            v2 = Th.triangle[elt_idx,(i + 1) % 3]

            # Sort vertices
            edge = tuple(sorted([v1, v2]))

            # Check if this edge appears in any other triangle of the cavity
            shared = False
            for kk in range(cavity.shape[0]):
                if kk == k:
                    continue

                elt_idx2 = cavity[kk]
                if v1 in Th.triangle[elt_idx2,:] and v2 in Th.triangle[elt_idx2,:]:
                    shared = True
                    break

            # If the edge is not shared, add it to the boundary edges set
            if not shared:
                edg_list.append(edge)

    cavity_nodes,cavity_edges = permute_cycle(edg_list)

    # Debug
    # plt.plot ( Th.node[cavity_nodes,0],Th.node[cavity_nodes,1] )
    # plt.show()
    # End debug

    return cavity_nodes,cavity_edges

def circle(Th,edg):
    """Computes center and ray of the circle circumscribed to edge edg"""

    c = 0.5 * (Th.node[edg[0],:] + Th.node[edg[1],:])
    r = 0.5 * np.linalg.norm(Th.node[edg[0],:]-Th.node[edg[1],:])

    return c,r


def circum_to_edges(Th,edg_list):
    """Computes center and ray of circle circumscribed to each edge
    of the list."""

    circle_list = list()
    for e in edg_list:
        c,r = circle(Th,e)
        circle_list.append((c,r))

    return circle_list

def circle_intersection(circle1, circle2):
    """Compute intersection points of circles circle1 and circle2,
    with circlei[0] the coordinates of the center of the circle circlei and
    circlei[1] its ray.

    """

    # Compute the distance between the centers of the circles
    c1 = circle1[0]
    r1 = circle1[1]
    c2 = circle2[0]
    r2 = circle2[1]

    d = np.linalg.norm(c1 - c2)

    # Check if the circles intersect
    if d >= r1 + r2:
        # Circles are too far apart to intersect or intersect at edge
        # extremities only (including equal values here avoid
        # numerical issues with a > r1 after)
        return []

    if d < abs(r1 - r2): # One circle is contained within the other
        print(colored("      ERROR: unexpected case: one of the circles is inscribed in another ",'red'))
        return []

    if d == 0 and r1 == r2: # Circles coincide, but are not distinct
        print(colored("      ERROR: unexpected case: combined circles ",'red'))
        return []

    # Compute the intersection points
    a = (r1*r1 - r2*r2 + d*d) / (2 * d)
    h = math.sqrt(r1*r1 - a*a)
    x = c1[0] + a * (c2[0] - c1[0]) / d
    y = c1[1] + a * (c2[1] - c1[1]) / d

    x1 = x + h * (c2[1] - c1[1]) / d
    y1 = y - h * (c2[0] - c1[0]) / d
    x2 = x - h * (c2[1] - c1[1]) / d
    y2 = y + h * (c2[0] - c1[0]) / d

    return [(x1, y1), (x2, y2)]

def is_inside_polygon(Th,p, edg_list):
    """Check if a point is inside a closed polygon defined by a list
    of edges using the ray tracing algorithm.

    Args:
    Th: input mesh
    p: tuple or list containing the (x,y) coordinates of the point to check.
    edg_list: list of tuples or lists, where each tuple or list contains
           the indices of the vertices of an edge in the polygon. The
           edges must form a closed polygon with no
           self-intersections.

    Returns:
    True if the point is inside the polygon, False otherwise.

    """
    # Count the number of intersections between a horizontal ray
    # starting from the point and the edges of the polygon.
    num_intersections = 0
    for i in range(len(edg_list)):
        # Get the indices of the vertices of the current edge.
        v0_coor = Th.node[edg_list[i][0]]
        v1_coor = Th.node[edg_list[i][1]]

        # Check if the point is on the same y-coordinate as the edge.
        if (p[1] > min(v0_coor[1], v1_coor[1]) and p[1] <= max(v0_coor[1], v1_coor[1])):
            # Calculate the x-coordinate of the intersection point
            # between the horizontal ray and the edge.
            x = (p[1]-v0_coor[1])*(v1_coor[0]-v0_coor[0])/(v1_coor[1]-v0_coor[1])+v0_coor[0]

            # If the intersection point is to the right of the point,
            # count it as a valid intersection.
            if x > p[0]:
                num_intersections += 1

    # If the number of intersections is odd, the point is inside the polygon.
    return num_intersections % 2 == 1

def is_inside_circle(p, c, r):
    """
    Check if a point is strictly inside a circle defined by its center and radius.

    Args:
    p: tuple or list containing the (x, y) coordinates of the point to check.
    c: tuple or list containing the (x, y) coordinates of the center of the circle.
    r: float representing the radius of the circle.

    Returns:
    True if the point is strictly inside the circle, False otherwise.
    """
    # Calculate the distance between the point and the center of the circle.
    v0 = p[0] - c[0]
    v1 = p[1] - c[1]
    d = v0*v0+v1*v1

    # Check if the distance is strictly less than the radius of the circle.
    return d < r*r-1.e-14


def admissible_intersections(Th,edg_list,cavity_points):
    """Compute the intersection points of all circles centered on
    polygon edges, then removes points outside the polygon or inside
    one of the other circle.

    """
    intersections = []

    circle_list = circum_to_edges(Th,edg_list)

    # Compute intersections of circles and keep intersections points
    # strictly inside the polygon formed by edg_list
    for i in range(len(circle_list)):
        c1 = circle_list[i]
        v1 = edg_list[i]

        for j in range(i+1,len(circle_list)):
            c2 = circle_list[j]
            v2 = edg_list[j]

            intersection = circle_intersection(c1, c2)

            # Remove intersection if it coincides with one of edge extremities
            for x in intersection:
                if np.linalg.norm(x-Th.node[v1[0]]) < 1e-14:
                    intersection.remove(x)
                elif np.linalg.norm(x-Th.node[v1[1]]) < 1e-14:
                    intersection.remove(x)
                elif np.linalg.norm(x-Th.node[v2[0]]) < 1e-14:
                    intersection.remove(x)
                elif np.linalg.norm(x-Th.node[v2[1]]) < 1e-14:
                    intersection.remove(x)

            # Add intersections that are inside our polygon
            for x in intersection:
                if is_inside_polygon(Th,x,edg_list):
                    intersections.append(x)

    # Now delete intersections that are inside one of the circumscribed circles
    for c,r in circle_list:
        for x in intersections:
            if is_inside_circle(x,c,r):
                intersections.remove(x)

    # Debug: draw cavity, circles and intersection points
    #plt.clf()
    #plt.plot(Th.node[edg_list,0],Th.node[edg_list,1])

    #for c,r in circle_list:
    #   circle1 = plt.Circle(c, r, color='r',fill=False)
    #   plt.gca().add_patch(circle1)

    #for x in intersections:
    #   plt.plot(x[0],x[1],'bo')

    #plt.autoscale()
    #plt.show()
    # End debug

    return intersections

def dKe_me_from_coor(tria_coor):
    """ Computes the regularity numbers dKe/me, and dKe/he for all edge e
    of all triangles K (2D only for now) from coordinates of triangles vertices.
    (copied from dKe_me function of fvm.py)
    """

    edges = set({(0, 1), (0, 2), (1, 2)})

    M = np.zeros((3, 2), dtype=np.float64)
    B = np.zeros((3, 1), dtype=np.float64)
    k = 0
    for (i, j) in edges:
        M[k, :] = tria_coor[j] - tria_coor[i]
        B[k, 0] = np.sum(M[ k, :] * 0.5 * (tria_coor[j] + tria_coor[i]), axis=0)
        k += 1
    # the QR decomposition is not vectorized, but the computation of
    # the More-Penrose pseudo-inverse is. We use the solution given by
    # this pseudo-inverse.
    xK = np.matmul(np.linalg.pinv(M), B).reshape(1, Th.dim)

    edges = set({(0, 1, 2), (2, 0, 1), (1, 2, 0)})
    dKe = np.zeros((3), dtype=np.float64)
    me = np.zeros((3), dtype=np.float64)
    M = np.zeros((2,2), dtype=np.float64)

    q = 0
    for (i, j, k) in edges:
        # i and j are the local indices of the vertices of the edge, k
        # is the index of the opposite vertex.
        #
        # det(xj-xi, xK-xi) = |xj-xi|*dist(xK,[xi,xj]) = |e|*dKe avec signe arbitraire
        #
        # det(xj-xi, xk-xi) = |xj-xi|*dist(xk,[xi,xj]) avec même signe
        # si xK est du côté intérieur
        #
        # i=A, j=B, k=C
        M[0,:] = tria_coor[j] - tria_coor[i] # AB
        me[q] = np.linalg.norm(M[0,:]) # |AB|
        M[1,:] = tria_coor[k] - tria_coor[i] # AC
        dABC = np.linalg.det(M)
        M[1,:] = xK - tria_coor[i] # AK
        dABK = np.linalg.det(M)
        dKe[q] = np.sign(dABK*dABC) * np.abs(dABK) / me[q]
        q += 1
    return dKe,me

def check_cavity_mesh(Th,cavity_edges,pt):
    """ Check admissibility of elements formed by the cavity boundaries
        and the new node """

    len_cavity = len(cavity_edges)
    triangle = np.empty((len_cavity,3),dtype = np.int32)

    dKe_me_min = np.finfo(np.float32).max
    n_invalid=0
    for i in range(len_cavity):
        e = cavity_edges[i]
        triangle[i] = [e[0],e[1],Th.n_nodes]
        triangle_coor = np.array([ Th.node[e[0]],Th.node[e[1]],pt  ])

        # Check elements admissibility
        dKe_loc,me_loc = dKe_me_from_coor(triangle_coor)
        dKe_me_loc = np.min(dKe_loc/me_loc)
        dKe_me_min = np.minimum(dKe_me_min,dKe_me_loc)

        if ( dKe_me_loc<= tol).any() :
            n_invalid += 1

    adm = (dKe_me_min > tol)
    return adm,triangle,dKe_me_min,n_invalid

def cavity_staring(Th,adja,cavity,cavity_edges,centroid,triangle):

    """ Delete cavity elements (stored in cavity), insert new node
    (stored in centroid) and creates the triangles fromed from the
    cavity boundaries (listed in cavity_edges) and this new
    point. These triangles have aloready been computed and are
    provided in the triangle variable. Update the array of adjacencies. """

    label = Th.triangle_label[cavity[0]]
    adj_indices = list()
    for e in cavity:
        adj_indices.append(adja[e,0])
        adj_indices.append(adja[e,1])
        adj_indices.append(adja[e,2])
        Th.del_elt(e)

    # Node and element insertion
    point_idx   = Th.add_node(centroid,0)
    elt_indices = list()
    for i in range(len(cavity_edges)):
        old_n_triangles_max = Th.n_triangles_max
        #label = 0 # Uncomment to see cavity with a different color
        elt_idx = Th.add_elt(triangle[i],label)
        elt_indices.append(elt_idx)

        if len(adja) < Th.n_triangles_max:
            adja.resize((Th.n_triangles_max,3),refcheck=False)

        # Update adjacency relationships
        # First deal with adjacencies through cavity boundaries
        v = cavity_edges[i]
        found = False
        for adj_idx in adj_indices:
            for jloc in range(3):
                if  Th.triangle[adj_idx,0] == -1 :
                    # deleted element
                    continue

                if ((Th.triangle[adj_idx,indices[jloc,0]] == v[0] and
                     Th.triangle[adj_idx,indices[jloc,1]] == v[1])
                    or (Th.triangle[adj_idx,indices[jloc,0]] == v[1] and
                        Th.triangle[adj_idx,indices[jloc,1]] == v[0])):
                    adja[elt_idx,2]    = adj_idx
                    adja[adj_idx,jloc] = elt_idx
                    found = True
                    break

            if found:
                break

    # Second deal with internal adjacencies
    for i in range(len(elt_indices)):
        j = (i+1)%len(elt_indices)
        adja[elt_indices[i],0] = elt_indices[j]
        adja[elt_indices[j],1] = elt_indices[i]

    return


def remove_obtuse(Th,output_filename,mmg_app,old_success,old_fail):
    """Process mesh elements, computes their admissibility and try to
    remove non-admissible elements."""

    if ( old_success == 0 ):
        remove_obtuse.no_success_count += 1
    else:
        remove_obtuse.no_success_count = 0

    adja = construct_adja(Th)

    # Delete mesh edges (can be reconstructed calling Mmg)
    Th.n_edges = 0

    # Get mesh info
    dim    = Th.dim
    n_elts = Th.n_triangles

    success = 0
    fail = 0
    n_obtuse_inserted = 0
    attempt_to_insert = 0
    for k in range(n_elts):

        # skip deleted or acute elements
        if Th.triangle[k,0]==-1:
            continue

        v = Th.triangle[k]
        triangle_coor = np.array([ Th.node[v[0]],Th.node[v[1]],Th.node[v[2]] ])

        # Check elements admissibility
        dKe_,me_ = dKe_me_from_coor(triangle_coor)
        adm =  (dKe_/me_ > tol).all()

        if adm:
            continue

        if verbosity > 1:
            print("   Attempt to remove obtuse angles in element ",k)

        # get non admissible element and first and second neighbours
        cavities = compute_cavities(Th,adja,k)

        mins_dKeme = list()
        centroids  = list()
        triangles  = list()
        cavities_edges = list()
        adms = list()
        for ic in range(len(cavities)):
            cavity = cavities[ic]

            # check if we were able to build the cavity: if not, try another cavity
            if np.min(cavity) < 0:
                centroids.append(None)
                adms.append(None)
                cavities_edges.append(None)
                triangles.append(None)
                continue

            # Check cavity building by coloring the mesh (to comment for a real usage)
            cav.append((cavity[0],cavity[1],cavity[2]))

            # Get boundary edges of the cavity
            cavity_points,cavity_edges  = compute_cavity_boundaries(Th,cavity)

            # Compute admissible intersections of circumscribe circles
            intersections = admissible_intersections(Th,cavity_edges,cavity_points)

            if len(intersections)==0 :
                # Test another of the admissible cavities
                if verbosity > 1 :
                    print(colored("      WARNING: unable to find a polygon for node insertion.",'yellow'))

                centroids.append(None)
                adms.append(None)
                cavities_edges.append(None)
                triangles.append(None)
                continue

            # Create node inside the polygon formed by the remaining points
            centroid = np.array(intersections).mean(axis=0)
            centroids.append(centroid)

            # Check admissibility of elements formed by the cavity boundaries
            # and the new node
            adm,triangle,min_dKeme,n_invalid = check_cavity_mesh(Th,cavity_edges,centroid)

            if not adm:
                assert ( n_invalid != 0 )
                # Debug
                #plt.plot(centroid[0],centroid[1],'go')
                #plt.show()
                # End debug

                # if no valid cavity: search the one creating the
                # least number of new acutes triangles (for centroid
                # point only)
                mins_dKeme.append((ic,min_dKeme,n_invalid))
                cavities_edges.append(cavity_edges)
                triangles.append(triangle)
                adms.append(adm)

                # Test each of the intersections points
                for centroid in intersections:
                    adm,triangle,min_dKeme,n_invalid = check_cavity_mesh(Th,cavity_edges,centroid)
                    if adm :
                        assert(n_invalid==0)
                        if verbosity > 1:
                            print( "      INFO: obtuse angle solved using one"
                                   " of intersections points.")

                        break

                if  not adm:
                    assert(n_invalid!=0)
                    if verbosity > 2:
                        # Test another of the admissible cavities
                        print(colored("      WARNING: tested new point creates obtuse angles..."
                                      " try with another cavity.",'yellow'))

                    continue

            # New elements are all acute, no need to continue to travel the cavities
            assert ( n_invalid==0 and adm )
            if  ic > 0 and verbosity > 2:
                print( "      INFO: obtuse angle solved using cavity number",ic)


            break

        if not adm:
            # Unable to find a valid config
            fail += 1

            if mins_dKeme:
                # Choose cavity that creates the lower num of invalid tria
                best_min_dKeme = min(mins_dKeme,key= lambda t:t[2])
            else:
                best_min_dKeme = (None,None,None)

            if verbosity > 1:
                print(colored("      WARNING: unable to find a cavity suitable"
                              " for acute angles.\n"
                              f"      Best cavity is cavity {best_min_dKeme[0] }:"
                              f" {best_min_dKeme[2]} new invalid tria,"
                              f" min(dKe/me) = { best_min_dKeme[1] }.",'yellow'))

            # This part of code is called only if we want to modify
            # cavities that creates obtuse angles. For now, we
            # authorize modifs every 3 iters because it slow down
            # convergency (by creating new obtuse angles) but it also
            # allows to solve som blocked situations.
            #
            # Authorize insertion only it it creates at most 1 new
            # obtuse tria and if situation is stucked
            if (mins_dKeme and (it+1) % obtuse == 0 and best_min_dKeme[2]==1
                and old_success==0 and remove_obtuse.mmg_remeshed==0 ):

                # Get back to the best cavity and insert the centroid
                cavity = cavities[best_min_dKeme[0]]
                centroid = centroids[best_min_dKeme[0]]
                # Get boundary edges of the cavity
                cavity_edges  = cavities_edges[best_min_dKeme[0]]
                triangle = triangles[best_min_dKeme[0]]
                adm = adms[best_min_dKeme[0]]
                n_obtuse_inserted += 1
                if verbosity > 1:
                    print(colored("      WARNING: Obtuse elements creation inside cavity.",'yellow'))
            else:
                # no treated for now
                continue
        else:
            # Found a valid config
            success += 1

        ## point insertion has succeed: delete invalid elements and insert new ones
        # element deletion and storage of indices of adjacent elements
        # If obtuse angle creation has been authorized, adm may be false
        cavity_staring(Th,adja,cavity,cavity_edges,centroid,triangle)

    # Check cavity building by coloring the mesh (to comment for a real usage)
    #for e1,e2,e3 in cav:
    #  Th.triangle_label[e1] = 6
    #  Th.triangle_label[e2] = 7
    #  Th.triangle_label[e3] = 8

    # Mesh packing (remove unused nodes and elements)
    Th.prune()

    # Remark: Can be improved, admissibility of elements is already at
    # least partially computed, we just need to store them and to
    # prune the array while pruning the mesh
    dKe,me = fvm.dKe_me(Th)
    is_admissible =  1*np.all(dKe/me > tol, axis=1)
    fail2 = np.count_nonzero(is_admissible==0)

    # Following assert is true only if cavities that creates new
    # obtuse angles are keeped untouched
    #assert( fail == fail2 )

    print(colored(f" |\n '---> {success} obtuse angles explicitely removed {fail2} remaining.\n",'cyan'))

    if fail2==0 :
        return success,fail2

    if debug:
        # Debug
        export_admissibility_to_mesh(Th,f"{output_filename}-it{it}")
        # End debug

    # If mesh has not been modified since previous iter, introduce
    # modifications to unblock stucked situations
    if n_obtuse_inserted != 0:
        print("        Stucked cavities meshed with new obtuse elements.")

    if ( success == 0 and n_obtuse_inserted == 0 and old_fail==fail2 and remove_obtuse.no_success_count >= 2 ):
        # Mesh freezing and creation of small meshes around invalid
        # elements to try to adapt them with Mmg
        Th.set_required_triangle(np.arange(Th.n_triangles))
        verts = Th.triangle[np.where(is_admissible==0)].flatten()

        for v in verts:
            Th.unset_required_triangle(np.where(Th.triangle == v )[0])

        if debug:
            # Debug
            export_tria_infos_to_mesh(Th,f"{output_filename}-it{it}-Mmg","required",1*Th.req_triangle)
            # End debug

        # Use mmg2D to adapt frozen parts of the mesh
        Th.export_to_file(f"{output_filename}-it{it}_Mmg.mesh")

        mmg_verbosity = -1

        system(f"{mmg_app} {output_filename}-it{it}_Mmg.mesh -v {mmg_verbosity} {mmg}"
               f" {output_filename}-it{it}_Mmg.o.mesh")
        Th.import_from_file(f"{output_filename}-it{it}_Mmg.o.mesh")

        # update static variables
        remove_obtuse.no_success_count = 0
        remove_obtuse.mmg_remeshed = 1

        if debug:
            # Debug
            export_admissibility_to_mesh(Th,f"{output_filename}-it{it}-Mmg")
            # End Debug
    else :
        remove_obtuse.mmg_remeshed = 0

    return success,fail2

def export_tria_infos_to_mesh(Th,output_filename,info_name,tria_infos):
    """Save information stored in an array of size n_triangles as
    material label over the triangles at Medit format.  the
    info_name string is appened to the output filename to allow to save
    different infos.
    Return the number of zeros in array.
    """

    label_save = Th.triangle_label
    Th.triangle_label = tria_infos
    Th.export_to_file(f"{output_filename}_{info_name}.mesh")

    Th.triangle_label = label_save

    return np.count_nonzero(tria_infos==False)


def export_admissibility_to_mesh(Th,output_filename):
    """ Save mesh admissibility as material label at Medit format.
    """

    dKe,me = fvm.dKe_me(Th)
    is_admissible =  1*np.all(dKe/me > tol, axis=1)

    count_zeros = export_tria_infos_to_mesh(Th,output_filename,"admissibility",is_admissible)
    return count_zeros

# -------------------------------------------------------------
# Main
# -------------------------------------------------------------
import argparse
import os
import re

# Command line parser
class Formatter( argparse.ArgumentDefaultsHelpFormatter,argparse.RawTextHelpFormatter): pass

parser = argparse.ArgumentParser(
    formatter_class=Formatter,
    description='Remove obtuse angles inside a 2D mesh.'
)

parser.add_argument("input_filename",
                    help='input filename with extension (.mesh or .vtk)')

parser.add_argument('-o','--out',dest='output_filename',
                    help='output filename with extension (.mesh or .vtk)',
                    default="acute.mesh")

parser.add_argument('-n', '--nitmax',default=59,
                    dest='nitmax',help='maximal number of iterations',
                    type=int)

parser.add_argument('-v', '--verbose',default=1,
                    dest='verbosity',help='verbosity level (1,2 or 3)',
                    type=int)

parser.add_argument('-t', '--tolerance',default=1e-3,
                    dest='tol',help='threshold for acute angle admissibility',
                    type=float)
parser.add_argument('-d', '--debug',
                    help='enable saving of temporary meshes for debugging purpose',
                    action=argparse.BooleanOptionalAction)

parser.add_argument('--mmg',default="\-optim -nofem",
                    help="command line arguments for Mmg (all mmg args can be used), example:\n"
                    "-optim: tries to preserve input edge lengths (lower num of collapses)\n"
                    "-nofem: do not force splitting of elements with more than"
                    " 1 boundary edge\n"
                    "-nosurf: no remeshing of boundaries\n")

parser.add_argument('--obtuse',default=10,
                    dest='obtuse',help='num of iter before a wave of creation of new obtuse angles',
                    type=int)


args = parser.parse_args()
input_filename  = args.input_filename
output_filename = args.output_filename
nitmax = args.nitmax
verbosity = args.verbosity
tol = args.tol
mmg = args.mmg
obtuse = args.obtuse
debug = args.debug

output_filename_t = os.path.splitext(output_filename)[0]
mmg_app = "~/mmg-dev/build/bin/mmg2d_debug"

Th = mesh.Mesh()

Th.import_from_file(input_filename)

if Th.dim == 3 :
    print(colored("      ERROR: 3D not implemented yet",'red'))
    exit_failure()

cav=list()

if debug:
    # Debug
    count = export_admissibility_to_mesh(Th,"initial")
    print(colored(f" |\n '---> {count} obtuse angles in mesh.\n",'cyan'))
    # End Debug

success = 1 # to avoid insertions creating obtuse elts at first iter.
fail = 0
remove_obtuse.no_success_count = 0
for it in range(nitmax):

    success,fail = remove_obtuse(Th,output_filename_t,mmg_app,success,fail)

    if fail == 0:
        print(colored(f" ACUTE MESH GENERATED IN {output_filename} FILE",'cyan'))
        break

if fail != 0:
    print(colored(f" FAIL TO GENERATE ACUTE MESH. SAVE FINAL MESH IN"
                  f" {output_filename} FILE",'red'))

# Debug
export_admissibility_to_mesh(Th,output_filename_t)
# End Debug

# Final mesh
Th.export_to_file(f"{output_filename}")
