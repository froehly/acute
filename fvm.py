# -*- coding: utf-8 -*-
"""
Created on Sept 26 2020

@author: Yves Coudière

Assembly of FV matrices

"""
from mesh import Mesh
import numpy as np
import scipy.sparse as sp

# Tolerance to detect zeros in this module. Currently used to check the
# geometriocal condition for the TPFA.
Tolerance = 1.e-8


def construct_centers(Th, method="isobarycenter"):
    """"Construct the centers of each FV cell in the mesh.
    
    Methods are "isobarycenter" or "circumscribed".
    
    """
    # Get the elements and the dimension of the interfaces
    if Th.dim == 2 or Th.is_surface:
        p = 2
        edges = set({(0, 1), (0, 2), (1, 2)})
        m = 3
        elt = Th.triangle
        n_elts = Th.n_triangles
    else:
        p = 3
        edges = set({(0, 1), (0, 2), (0, 3), (1, 2), (1, 3), (2, 3)})
        m = 6
        elt = Th.tetra
        n_elts = Th.n_tetras
    # p is the geometrical dimension, and m is the number of edges of an
    # elt.

    if method == 'isobarycenter':
        x = np.sum(Th.node[elt], axis=1) / (p + 1)
    elif method == 'circumscribed':
        # The center of the circumbscribed sphere is x such that
        # (x-0.5*(xi+xj)) is perpendicular to (xj-xi) for edge (xi,xj) of
        # the simplex. There are 3 edges for triangles and 6 for
        # tetras. The number of coordinates is 2 or 3 for triangles and 3
        # for tetras. More generally, the number of coordinates is d and
        # we consider a p-simplex, with d>=p. Hence there are
        # (p+1)!/2/(p-1)! equations and d unknowns. We have then
        #
        # p=d=2: system 3x2 with rank 2, unique solution
        #
        # p=2<d=3: system 3x3 with rank 2, infinite number of solution,
        # but we look for the min |MX-B|
        #
        # p=d=3: system 6x3 with rank 3, unique solution
        #
        # In all cases, the QR decomposition yields the required solution
        M = np.zeros((n_elts, m, Th.dim), dtype=np.float64)
        B = np.zeros((n_elts, m, 1), dtype=np.float64)
        k = 0
        for (i, j) in edges:
            M[:, k, :] = Th.node[elt[:n_elts, j]] - Th.node[elt[:n_elts, i]]
            B[:, k, 0] = np.sum(M[:, k, :] * 0.5 * (Th.node[elt[:n_elts, j]] + Th.node[elt[:n_elts, i]]), axis=1)
            k += 1
        # the QR decomposition is not vectorized, but the computation of
        # the More-Penrose pseudo-inverse is. We use the solution given by
        # this pseudo-inverse.
        x = np.matmul(np.linalg.pinv(M), B).reshape(n_elts, Th.dim)
    else:
        print("Unknown method {}".format(method))
        return
    return x


def reorient(Th,xK):
    """Check that all interfaces are well oriented, meaning that for
    x1...xd := itf[:,0:d] and xK,xL := itf[:,d:], we have that
    det(xL-xK,xd-x1...x2-x1)>0, otherwise, exchange xd and x{d-1}, so
    that it becomes true.

    """
    d = Th.dim
    x_S = Th.node[Th.interface[:,:d]] # array n_itfs x d x d
    x_K = xK[Th.interface[:,d]] # array n_tifs x d
    x_L = np.zeros_like(x_K)
    i_int = np.where(Th.interface[:,d+1] > -1) # interior interfaces
    x_L[i_int] = xK[Th.interface[i_int,d+1]]
    i_bnd = np.where(Th.interface[:,d+1] == -1) # boundary interfaces
    x_L[i_bnd] = np.sum(x_S[i_bnd],axis=1) / d # isobarycenter of the interface

    A = np.zeros_like(x_S)
    A[:,0,:] = x_L - x_K
    for i in np.arange(1,d):
        A[:,i,:] = x_S[:,i,:] - x_S[:,0,:]

    det = np.linalg.det(A) # det for all diamond cells -> array of size
                           # n_itfs
    i_chg = np.where(det<0) # indices where nodes have to be swapped
    tmp = np.copy(Th.interface[i_chg,d-1])
    Th.interface[i_chg,d-1] = Th.interface[i_chg,d-2]
    Th.interface[i_chg,d-2] = np.copy(tmp)

    print ("{} interfaces were re-oriented, all interfaces are direct now".format(np.size(i_chg)))
    return 
    
def tpfa_isotropic_matrix(Th, x, sigma=np.empty(0, dtype=np.float64)):
    """Build the two point flux approximation matrix for a homogeneous
    Neumann condition. The centers of the mesh cell are in the array x,
    and assumed to be such that the orthogonality condition holds true
    on all interfaces.
    
    The optional parameter sigma is a conductivity value in each mesh
    element.

    """
    n_itfs = Th.n_interfaces
    # In the isotropic case, the distance is |xC-xe| for C=K or L and
    # e=K|L, where xe is the circumbscribed point for the vertices of the
    # interface e. Let's do like in the function 'compute centers'.
    if Th.dim == 2 or Th.is_surface:
        d = 2
        # xe is just themidpoint of each interface
        xe = 0.5 * (Th.node[Th.interface[:, 0]] + Th.node[Th.interface[:, 1]])
    else:
        d = 3
        # xe is the circumbscribed center of the triangular interface
        edges = set({(0, 1), (0, 2), (1, 2)})
        M = np.zeros((n_itfs, Th.dim, Th.dim), dtype=np.float64)
        B = np.zeros((n_itfs, Th.dim, 1), dtype=np.float64)
        k = 0
        for (i, j) in edges:
            M[:, k, :] = Th.node[Th.interface[:, j]] - Th.node[Th.interface[:, i]]
            B[:, k, 0] = np.sum(M[:, k, :] * 0.5 * (
                    Th.node[Th.interface[:, j]] + Th.node[interface[:, i]]), axis=1)
            k += 1
        # the QR decomposition is not vectorized, but the computation of
        # the More-Penrose pseudo-inverse is. We use the solution given by
        # this pseudo-inverse.
        xe = np.matmul(np.linalg.pinv(M), B).reshape(n_itfs, Th.dim)
    # Now xe are the centers of the interfaces
    dCe = np.zeros((n_itfs, 2), dtype=np.float64)

    xC = x[Th.interface[:, d]]
    dCe[:, 0] = np.linalg.norm(xe - xC, axis=1)  # d(xK,xe)

    boundary = Th.interface[:, d + 1] == -1
    for i in np.arange(Th.dim):
        # Take xC everywhere and xe on the boundary
        xC[:, i] = np.where(boundary, xe[:, i], x[Th.interface[:, d + 1], i])
    dCe[:, 1] = np.linalg.norm(xe - xC, axis=1)  # d(xL,xe)

    # Assert the all distance are >0 (tolerance), except if they are on
    # the boundary
    h = Th.computeMaxLengthEdges()
    ok_K = np.all(dCe[:, 0] > Tolerance * h)
    ok_L = np.all(np.where(boundary, True, dCe[:, 1] > Tolerance * h))
    assert ok_K and ok_L, "Some cell centers are too close to an interface -- the mesh quality os not correct for this method"

    # Compute the transmitivity tau_ke = sigma_k/d_ke for all elt k and
    # its edges/faces e.
    #
    # We need 1 conductivity on each neighbour of each interface, but
    # have access to sigma, which is either empty or the conductivity in
    # each mesh cell.
    if sigma.size == 0:
        sigmaCe = np.ones_like(dCe)  # use 1 everywhere
    else:
        sigmaCe = np.zeros_like(dCe)
        sigmaCe[:, 0] = sigma[Th.interface[:, d]]
        sigmaCe[:, 1] = np.where(boundary, -1., sigma[Th.interface[:, d + 1]])
    # Compute the transmitivity
    sigmaCe = dCe / sigmaCe  # 0 where dCe=0  (e.g. on the boundary)
    tau = np.zeros(n_itfs, dtype=np.float64)
    tau = 1. / (sigmaCe[:, 0] + sigmaCe[:, 1])
    return tau

def dKe_me(Th):
    """Computes the regularity numbers dKe/me, and dKe/he for all edge e
    of all triangles K (2D) only for now.

    """
    # Get the elements and the dimension of the interfaces
    if Th.dim == 2 or Th.is_surface:
        p = 2
        # Thes edges have to be in direct order for the sign trick below
        # to be correct
        edges = set({(0, 1, 2), (2, 0, 1), (1, 2, 0)})
        m = 3
        elt = Th.triangle
        n_elts = Th.n_triangles
    else:
        elt = Th.tetra
        n_elts = Th.n_tetras
        print ("WARNING: 3D not implemented yet")
        return 
    # p is the geometrical dimension, and m is the number of edges of an
    # elt.

    
    # First, we need the xK
    xK = construct_centers(Th, method="circumscribed")
    # We compute by triangle the distance xK to each edge e, it is
    # negative if the center is outside of K with respect to e.
    #
    # This is an array of size n_elts by m
    dKe = np.zeros((n_elts,m), dtype=np.float64)
    me = np.zeros((n_elts,m), dtype=np.float64)
    M = np.zeros((n_elts, 2,2), dtype=np.float64)
    q = 0
    for (i, j, k) in edges:
        # i and j are the local indices of the vertices of the edge, k
        # is the index of the opposite vertex.
        #
        # det(xj-xi, xK-xi) = |xj-xi|*dist(xK,[xi,xj]) = |e|*dKe avec signe arbitraire
        #
        # det(xj-xi, xk-xi) = |xj-xi|*dist(xk,[xi,xj]) avec même signe
        # si xK est du côté intérieur
        #
        # i=A, j=B, k=C
        M[:,0,:] = Th.node[elt[:n_elts,j]] - Th.node[elt[:n_elts,i]] # AB
        me[:,q] = np.linalg.norm(M[:,0,:],axis=1) # |AB|
        M[:,1,:] = Th.node[elt[:n_elts,k]] - Th.node[elt[:n_elts,i]] # AC
        dABC = np.linalg.det(M)
        M[:,1,:] = xK - Th.node[elt[:n_elts,i]] # AK
        dABK = np.linalg.det(M)
        dKe[:,q] = np.sign(dABK*dABC) * np.abs(dABK) / me[:,q]
        q += 1
    return dKe,me

def create_carre_1x1(n):
    """Creates a triangulation of the (0,1) x (0,1) square, with n
    subdivisions in each directions as a starting point. The qhull
    wrapper function from scipy.spatial is used, with default arguments.

    """
    from os import system
    from tempfile import NamedTemporaryFile
    Th = Mesh()
    Th.dim = 2
    from scipy.spatial import Delaunay
    # Construct a list of nodes regularly spaced
    x = y = np.arange(n+1)/n
    X,Y = np.meshgrid(x,y)
    Th.node = np.transpose(np.array([X.flatten(), Y.flatten()]))
    Th.n_nodes = Th.node.shape[0]
    Th.node_label = np.zeros(Th.n_nodes, dtype=np.int)
    # Use them to generate a triangulation with qhull
    tri = Delaunay(Th.node)
    Th.triangle = tri.simplices
    Th.n_triangles = Th.triangle.shape[0]
    Th.triangle_label = np.zeros(Th.n_triangles, dtype=np.int)
    # Write the mesh to a temp file
    fid_in = NamedTemporaryFile(suffix=".mesh")
    Th.export_to_file(fid_in.name)
    # Use mmg2D to optimize the mesh, and write the optimized mesh to a
    # new temp file
    fid_out = NamedTemporaryFile(suffix=".mesh")
    system("mmg2d_O3 {} -optim -out {}".format(fid_in.name, fid_out.name))
    # Read the temp file, this is the final mesh
    Th_opt = Mesh()
    Th_opt.import_from_file(fid_out.name)
    return Th_opt
        
def create_oblique_1x1(n):
    """Creates a triangulation of the (0,1) x (0,1) square, with n
    subdivisions in each directions as a starting point. The qhull
    wrapper function from scipy.spatial is used, with default arguments.

    """
    from os import system
    from tempfile import NamedTemporaryFile
    Th = Mesh()
    Th.dim = 2
    from scipy.spatial import Delaunay
    # Construct a list of nodes regularly spaced
    x = y = np.arange(n+1)/n
    X,Y = np.meshgrid(x,y)
    ux = np.array([1.,0.])
    uy = np.array([0.5,0.5*np.sqrt(3)])
    Xp = X*ux[0] + Y*uy[0]
    Yp = X*ux[1] + Y*uy[1]
    Th.node = np.transpose(np.array([Xp.flatten(), Yp.flatten()]))
    Th.n_nodes = Th.node.shape[0]
    Th.node_label = np.zeros(Th.n_nodes, dtype=np.int)
    # Use them to generate a triangulation with qhull
    tri = Delaunay(Th.node)
    Th.triangle = tri.simplices
    Th.n_triangles = Th.triangle.shape[0]
    Th.triangle_label = np.zeros(Th.n_triangles, dtype=np.int)
    return Th
        
def isAdmissible(Th,tolerance=1.e-3):
    """Check that the mesh is admissible for TPFA. The tolerance is
    optional. An edge is not admissible of dKe/me < tolerance

    """
    dKe,me = dKe_me(Th)
    print("Min et max de dKe/me : {} {}".
          format(np.min(np.abs(dKe/me)),np.max(np.abs(dKe/me))))
    non_adm = np.where( dKe/me < tolerance )[0]
    is_admissible = not np.any(non_adm)
    if not is_admissible:
        print("List of the {} elements with xK outside of K: \n{}".
              format(non_adm.shape[0],non_adm))
    return is_admissible
              
